<!--
SPDX-FileCopyrightText: 2021 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

## Workshop Setup

Here you find basic information about the workshop setup.

## Plan the Schedule

We use the following schedule for an online setup of the workshop.
Please modify times and dates according to your needs.

## Day 1 - (DD.MM.YYYY)
- 09:00 - 09:30 Welcome & Introduction
- 09:30 - 10:30 Episodes 1 - 4: Using the Shell, Introduction to Version Control, Setting up Git, Creating a Repository
- 10:30 - 12:45 Episodes 5 - 7: Tracking Changes, Exploring History, Ignoring Things
- 12:45 - 13:00 Wrap up & Feedback

## Day 2 - (DD.MM.YYYY)
- 09:00 - 09:30 Welcome & Introduction
- 09:30 - 11:00 Episode 8: Branching and Conflicts
- 11:00 - 12:00 Episode 9: Remotes with GitLab
- 12:00 - 12:45 Episode 10: Collaboration with Others
- 12:45 - 13:00 Wrap up & Feedback

## Setup the Pads

The directory [pads](pads) contains pad templates that we usually set up in a service such as [HedgeDoc](https://hedgedoc.org/):
- Throughout the whole workshop, we use the [Main Pad](./setup/pads/main.md).
- Please copy and modify the pad to your needs
